/*
# gen doc
npm i jsdoc-to-markdown -g
jsdoc2md index.js > docs/api.md
*/

/**
 * @typedef CallSite
 * @type {object}
 * @property {object | undefined} getThis()
 * @property {string} getTypeName()
 * @property {Function | undefined} getFunction()
 * @property {string} getFunctionName()
 * @property {string | null} getMethodName()
 * @property {string | undefined} getFileName()
 * @property {number} getLineNumber()
 * @property {number} getColumnNumber()
 * @property {CallSite | string} getEvalOrigin()
 * @property {boolean} isToplevel()
 * @property {boolean} isEval()
 * @property {boolean} isNative()
 * @property {boolean} isConstructor()
 */

/**
 * @typedef StackItem
 * @type {object}
 * @property {string} method - Name of function on stack
 * @property {number} line - Line number on stack
 * @property {string} file - /PathOfFile/Source/NameOfFilename.js
 * @property {string} filename - NameOfFile
 * @property {CallSite[]} callSites - /PathOfFile/Source/NameOfFilename.js
 */

/**
 * @module lineStack
 * @example
 * const lineStack = require('line-stack')
 */

/**
 * @alias module:lineStack
 * @typicalname lineStack
 */

class LineStack {
  /**
   * Returns a single item at given level
   *
   * @param {number} [level] Useful to return levels up on the stack. If not informed, the first (0, zero index) element of the stack will be returned
   * @returns {StackItem}
   */
  get (level = 0) {
    const stack = getStack();
    const i = Math.min(level + 1, stack.length - 1);
    const item = stack[i];
    if (!!item) return parse(item, stack);
  }
  
  /**
   * find the stack using filename that matches the given RegExp or string.
   * @param rx {string|RegExp} - string or regular expression to use to find the item
   * @param fromIndex {number} - the index of the first item to find
   * @param stack {CallSite[]} - array of StackItems to search through
   * @param returnIndex {boolean} - if true, return the stack index the item is instead of the item
   * @return {StackItem|number|void}
   */
  findByFilename (rx, fromIndex = 0, stack = getStack(), returnIndex = false) {
    return iterateStack(stack, fromIndex, returnIndex, (item) =>
        (
            item.getScriptNameOrSourceURL && item.getScriptNameOrSourceURL() ||
            item.getEvalOrigin && item.getEvalOrigin() ||
            item.getFileName && item.getFileName() ||
            ''
        ).match(rx),
    );
  }
  
  /**
   * find the next stack using filename that doesn't match the given string.
   * @param matcher {string}
   * @return {StackItem|void}
   */
  skipByFilename (matcher) {
    const stack = getStack();
    const startIndex = this.findByFilename(new RegExp(`${matcher}.*$`), 0, stack, true);
    if (~startIndex) return this.findByFilename(new RegExp(`(?<!${matcher}.*)$`), startIndex, stack, false);
  }
  
  /**
   * find the stack using type name that matches the given RegExp or string.
   * @param rx {string|RegExp} - string or regular expression to use to find the item
   * @param fromIndex {number} - the index of the first item to find
   * @param stack {CallSite[]} - array of StackItems to search through
   * @param returnIndex {boolean} - if true, return the stack index the item is instead of the item
   * @return {StackItem|number|void}
   */
  findByTypeName (rx, fromIndex = 0, stack = getStack(), returnIndex = false) {
    return iterateStack(stack, fromIndex, returnIndex, (item) => (item.getTypeName() || '').match(rx));
  }
  
  /**
   * find the next stack using type name that doesn't match the given string.
   * @param matcher {string}
   * @return {StackItem|void}
   */
  skipByTypeName (matcher) {
    const stack = getStack();
    const startIndex = this.findByTypeName(new RegExp(`${matcher}.*$`), 0, stack, true);
    if (~startIndex) return this.findByTypeName(new RegExp(`(?<!${matcher}.*)$`), startIndex, stack, false);
  }
  
  /**
   * find the stack using function name that matches the given RegExp or string.
   * @param rx {string|RegExp} - string or regular expression to use to find the item
   * @param fromIndex {number} - the index of the first item to find
   * @param stack {CallSite[]} - array of StackItems to search through
   * @param returnIndex {boolean} - if true, return the stack index the item is instead of the item
   * @return {StackItem|number|void}
   */
  findByFunctionName (rx, fromIndex = 0, stack = getStack(), returnIndex = false) {
    return iterateStack(stack, fromIndex, returnIndex, (item) => (item.getFunctionName() || '').match(rx));
  }
  
  /**
   * find the next stack using function name that doesn't match the given string.
   * @param matcher {string}
   * @return {StackItem|void}
   */
  skipByFunctionName (matcher) {
    const stack = getStack();
    const startIndex = this.findByFunctionName(new RegExp(`${matcher}.*$`), 0, stack, true);
    if (~startIndex) return this.findByFunctionName(new RegExp(`(?<!${matcher}.*)$`), startIndex, stack, false);
  }
  
  
  /**
   * Returns all stack
   *
   * @returns {CallSite[]}
   */
  all () {
    const stack = getStack();
    // console.debug(`[line-stack]-51: all() - stack: %o`, stack);
    const result = [];
    for (let i = 1; i < stack.length - 1; i++) {
      const item = stack[i];
      if (!!item) result.push(parse(item, stack));
    }
    return result;
  }
}

/**
 * Get the call callSites stack
 * @return {CallSite[]}
 */
function getStack () {
  const orig = Error.prepareStackTrace;
  Error.prepareStackTrace = function (_, stack) {
    return stack;
  };
  const err = new Error();
  // noinspection JSAnnotator
  Error.captureStackTrace(err, arguments.callee);
  const stack = err.stack;
  Error.prepareStackTrace = orig;
  // console.debug(`[index]-49: getStack() - stack: %o`, stack);
  // console.debug(`[line-stack]-153: getStack() - name: %o`, stack[0].getTypeName());
  // always throw away the first stack item if it's for this module
  if (stack[0].getTypeName() === 'LineStack') stack.shift();
  // noinspection JSValidateTypes
  return stack;
}

/**
 * Parse the given stack item
 * @param item
 * @param stack
 * @return {StackItem}
 */
function parse (item, stack) {
  const result = {
    method:    item.getFunctionName(),
    line:      item.getLineNumber(),
    file:      item.getFileName() ||
                   item.getScriptNameOrSourceURL && item.getScriptNameOrSourceURL() ||
                   item.evalOrigin && item.evalOrigin(),
    filename:  undefined,
    callSites: stack,
  };
  // console.debug(`[line-stack]-83: parse() - result`, result);
  // todo properly remove the extension
  if (!!result.file) result.filename = result.file.replace(/^.*\/|\\/gm, '').replace(/\.\w+$/gm, '');
  return result;
}

/**
 * iterate through given stack items and return of first item where predicate returns truthy.
 * @param stack {CallSite[]} - stack to iterate through
 * @param fromIndex {number}  - the index to start at
 * @param returnIndex {boolean} - if true, return the stack index the item is instead of the item
 * @param predicate {Function} - predicate function to use. called with arguments (item) - the current stack item
 * @return {StackItem|number|void}
 */
function iterateStack (stack, fromIndex, returnIndex, predicate) {
  for (let i = fromIndex; i < stack.length - 1; i++) {
    const item = stack[i];
    if (!item) continue;
    
    if (!!predicate(item)) {
      if (returnIndex) return i;
      
      return parse(item, stack);
    }
  }
}


export default new LineStack();
